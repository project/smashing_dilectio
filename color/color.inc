<?php
 
$info = array(

  // Available colors and color labels used in theme.
  'fields' => array(
    'base' => t("Base"),
    'text' => t('Text'),
    'link' => t('Link'),
    'linkhover' => t('Hovered Link'),
    'block' => t('Block background'),
    'separator' => t('List item separator'),
    'title' => t('Titles'),
  ),

  // Pre-defined color schemes.
  'schemes' => array(
    'default' => array(
      'title' => t('Red (default)'),
      'colors' => array(
        'base' => '#f3f8e2',
        'text' => '#63565f',
        'link' => '#cc0000',
        'linkhover' => '#ff6600',
        'block' => '#fff6e8',
        'separator' => '#ffd589',
        'title' => '#660000',
      ),
    ),
    'green' => array(
      'title' => t('Green'),
      'colors' => array(
        'base' => '#fdfcfc',
        'text' => '#63565F',
        'link' => '#a7b215',
        'linkhover' => '#eb7e05',
        'block' => '#f8fae8',
        'separator' => '#daff89',
        'title' => '#404040',
      ),
    ),
    'blue' => array(
      'title' => t('Blue'),
      'colors' => array(
        'base' => '#fbfdfe',
        'text' => '#63565f',
        'link' => '#009fcc',
        'linkhover' => '#e63f19',
        'block' => '#ebf6fa',
        'separator' => '#b4dae9',
        'title' => '#363636',
      ),
    ),
    'pink' => array(
      'title' => t('Pink'),
      'colors' => array(
        'base' => '#fceef6',
        'text' => '#63565f',
        'link' => '#cc006b',
        'linkhover' => '#fa583d',
        'block' => '#fbeef4',
        'separator' => '#e9b4d1',
        'title' => '#363636',
      ),
    ),
  ),
 
  // Images to copy over.
  'copy' => array(
    'logo.png',
  ),
 
  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => array(
    'color.css',
  ),

  // Gradient definitions.
  'gradients' => array(
    array(
      // (x, y, width, height).
      'dimension' => array(100, 100, 200, 20),
      // Direction of gradient ('vertical' or 'horizontal').
      'direction' => 'vertical',
      // Keys of colors to use for the gradient.
      'colors' => array('link', 'text'),
    ),
  ),

  // Color areas to fill (x, y, width, height).
  'fill' => array(
    'link' => array(0, 0, 63, 61),
  ),
 
  // Coordinates of all the theme slices (x, y, width, height)
  // with their filename as used in the stylesheet.
  'slices' => array(
    'logo.png' => array(0, 0, 63, 61),
  ),
 
  // Reference color used for blending. Matches the base.png's colors.
  'blend_target' => '#ffffff',
 
  // Preview files.
  'preview_image' => 'color/preview.png',
  'preview_css' => 'color/preview.css',
 
  // Base file for image generation.
  'base_image' => 'color/base.png',
);
